package fr.cnam.foad.nfa035.dao;

import fr.cnam.foad.nfa035.fileutils.streaming.media.ImageByteArrayFrame;
import fr.cnam.foad.nfa035.fileutils.streaming.media.ImageFileFrame;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageDeserializerBase64StreamingImpl;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageSerializerBase64StreamingImpl;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageStreamingDeserializer;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageStreamingSerializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;


public class BadgeWalletDAO {

    private static final Logger LOG = LogManager.getLogger(BadgeWalletDAO.class);
    private File walletDatabase;


    /**
     * @param cheminImage
     */
    public BadgeWalletDAO(String cheminImage) throws IOException {
        this.walletDatabase = new File(cheminImage);


    }

    /**
     * @param image
     * @throws IOException
     */
    public void addBadge(File image) throws IOException {

        ImageFileFrame media = new ImageFileFrame(walletDatabase);

        ImageStreamingSerializer serializer = new ImageSerializerBase64StreamingImpl();
        serializer.serialize(image, media);
    }

    /**
     * @param imageStream
     * @throws IOException
     */
    public void getBadge(OutputStream imageStream) throws IOException{

        ByteArrayOutputStream deserializationOutput = new ByteArrayOutputStream();
        ImageStreamingDeserializer deserializer = new ImageDeserializerBase64StreamingImpl(deserializationOutput);

        deserializer.deserialize(imageStream);
    }

}
